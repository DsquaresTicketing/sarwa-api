﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SawraAPIs.Enums
{
    public enum PackageErrorsEnum
    {
        InvalidSession = 809,
        Customeraccountisdeactivatedorsuspended=805,
        Availablebalanceinsufficient = 812,
        CustomerDosntExist = 814
    }
}