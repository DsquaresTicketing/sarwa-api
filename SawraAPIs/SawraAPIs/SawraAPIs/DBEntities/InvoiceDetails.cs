﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SawraAPIs.DBEntities
{
    public class InvoiceDetails
    {
        [Key]
        public int Id { get; set; }
        public int ItemId { get; set; }
        public int Price { get; set; }
        public int DownPayment { get; set; }
        public int InstallmentSeqNo { get; set; }
        public int Tenor { get; set; }
        public int? RefundNo { get; set; }
        public DateTime? RefundDate { get; set; }
        public bool IsBonus { get; set; }
        [ForeignKey("Invoice")]
        public int InvoiceNo { get; set; }
        public Invoice Invoice { get; set; }
    }
}