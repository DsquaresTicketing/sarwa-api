﻿using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http.Filters;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http.Controllers;
using SawraAPIs.HelperClassess;
using SawraAPIs.Models.requestObjects;
using System.Security.Principal;

namespace SawraAPIs.FilterAction
{

    public class SessionRefresherFilter : System.Web.Http.Filters.ActionFilterAttribute
    {
        private readonly SessionChecker _sessionChecker  = new SessionChecker();
        
        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            object obj = null;
            User userDataObj = new User();
            if (actionContext.ActionArguments.TryGetValue("sessionId", out obj))
            {
                //bool isAuthorized = false;
                string sessionId = (string)obj;
                if (actionContext.Request.Headers.Authorization != null)
                {
                    var authToken = actionContext.Request.Headers.Authorization.Parameter;
                    var decodeauthToken = System.Text.Encoding.UTF8.GetString(
                        Convert.FromBase64String(authToken));

                    // spliting decodeauthToken using ':'   
                    var arrUserNameandPassword = decodeauthToken.Split(':');

                    // at 0th postion of array we get username and at 1st we get password  
                    if (arrUserNameandPassword[0] != null && arrUserNameandPassword[1] != null)
                    {
                        userDataObj.Username = arrUserNameandPassword[0];
                        userDataObj.Password = arrUserNameandPassword[1];
                        // setting current principle  
                        actionContext.ActionArguments["sessionId"] = _sessionChecker.CheckSessionExpiry(sessionId, (User)userDataObj);

                    }
                    else
                    {
                        actionContext.Response = actionContext.Request
                            .CreateResponse(System.Net.HttpStatusCode.Unauthorized);
                    }
                }
                else
                {
                    actionContext.Response = actionContext.Request
                        .CreateResponse(System.Net.HttpStatusCode.Unauthorized);
                }
                   

               
                //if(!isAuthorized)
                //    actionContext.Response = actionContext.Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
            }
            else
                actionContext.Response = actionContext.Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
        }
    }
}