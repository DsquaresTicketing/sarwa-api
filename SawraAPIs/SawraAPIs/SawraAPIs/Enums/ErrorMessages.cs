﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace SawraAPIs.Enums
{
    public enum ErrorMessages
    {
        [Description("Success")]
        success = 0,
        [Description("Error Occured")]
        error_occured = -99,
        [Description("Otp message did not send")]
        otpMessageError = -1,
        [Description("Otp is invalid")]
        invalidOtp = -2,
        [Description("Internal Db Error Please Contact Your Adminstrator")]
        InternalDbError = -3
    }
}