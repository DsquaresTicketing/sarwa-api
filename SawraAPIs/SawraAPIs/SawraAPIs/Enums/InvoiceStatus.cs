﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SawraAPIs.Enums
{
    public enum InvoiceStatus
    {
        Default = 1, 
        Refunded = 2,
        PartialRefunded = 3
    }
}