namespace SawraAPIs.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class createDb : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.InvoiceDetails",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ItemId = c.Int(nullable: false),
                        Price = c.Int(nullable: false),
                        DownPayment = c.Int(nullable: false),
                        InstallmentSeqNo = c.Int(nullable: false),
                        Tenor = c.Int(nullable: false),
                        RefundNo = c.Int(),
                        RefundDate = c.DateTime(),
                        IsBonus = c.Boolean(nullable: false),
                        InvoiceNo = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Invoices", t => t.InvoiceNo, cascadeDelete: true)
                .Index(t => t.InvoiceNo);
            
            CreateTable(
                "dbo.Invoices",
                c => new
                    {
                        InvoiceNo = c.Int(nullable: false),
                        Price = c.Decimal(nullable: false, precision: 18, scale: 2),
                        DownPayment = c.Decimal(nullable: false, precision: 18, scale: 2),
                        ClientId = c.String(),
                        CreationDate = c.DateTime(nullable: false),
                        Status = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.InvoiceNo);
            
            CreateTable(
                "dbo.SMSCredentials",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        mobileNo = c.String(),
                        code = c.String(),
                        expirationDate = c.DateTime(nullable: false),
                        isValid = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.UserSessions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        SessionId = c.String(),
                        Username = c.String(),
                        ExpiryTime = c.DateTime(nullable: false),
                        IsExpired = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.InvoiceDetails", "InvoiceNo", "dbo.Invoices");
            DropIndex("dbo.InvoiceDetails", new[] { "InvoiceNo" });
            DropTable("dbo.UserSessions");
            DropTable("dbo.SMSCredentials");
            DropTable("dbo.Invoices");
            DropTable("dbo.InvoiceDetails");
        }
    }
}
