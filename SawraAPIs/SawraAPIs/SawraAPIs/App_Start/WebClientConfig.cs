﻿using Newtonsoft.Json;
using Sarwa.Models;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web;

namespace Sarwa.App_Start
{
    public class WebClientConfig
    {
       // private static WebClient client = new WebClient();
        private WebClientConfig() { }
        //public static string ManageAPIGet(string Path, Dictionary<string, string> apiParams)
        //{


        //    using (WebClient client = new WebClient())
        //    {
        //        string URL = ConfigurationManager.AppSettings["ApiUrl"].ToString();
        //        client.BaseAddress = $"{URL}{Path}";
        //        client.Headers.Clear();
        //        client.Headers.Add("Content-Type", "application/json");

        //        for (var i = 0; i < apiParams.Count; i++)
        //        {
        //            client.QueryString.Add(apiParams.Keys.ElementAt(i), apiParams.Values.ElementAt(i));
        //        }

        //        var reqString = Encoding.Default.GetBytes(JsonConvert.SerializeObject(apiParams, Formatting.Indented));

        //        var HtmlResult = client.UploadData(client.BaseAddress, reqString);
        //        var responseString = UnicodeEncoding.UTF8.GetString(HtmlResult);
        //        return responseString;
        //    }

        //}
        public static string ManageAPI<TRequest>(string path, TRequest apiParams) 
        {
            using (WebClient client = new WebClient())
            {
                string URL = ConfigurationManager.AppSettings["ApiUrl"].ToString();
                client.BaseAddress = $"{URL}{path}";
                client.Headers.Clear();
                client.Headers.Add("Content-Type", "application/json");

                var reqString = Encoding.Default.GetBytes(JsonConvert.SerializeObject(apiParams, Formatting.Indented));
                var HtmlResult = client.UploadData(client.BaseAddress, reqString);
                var responseString = UnicodeEncoding.UTF8.GetString(HtmlResult);
                    return responseString;
            }

        }

    }
}
