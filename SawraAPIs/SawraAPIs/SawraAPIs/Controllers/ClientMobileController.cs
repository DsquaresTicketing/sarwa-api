﻿using Newtonsoft.Json;
using Sarwa.App_Start;
using SawraAPIs.Enums;
using SawraAPIs.FilterAction;
using SawraAPIs.HelperClassess;
using SawraAPIs.Models.requestObjects;
using SawraAPIs.Models.responseObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace SawraAPIs.Controllers
{
   // [EnableCors(origins: "*", headers: "*", methods: "*")]

    public class ClientMobileController : ApiController
    {
        // GET api/<controller>
        [HttpGet]
        [SessionRefresherFilter]
        public IHttpActionResult GetClientMobile(string sessionId, string clientId)
        {
            try
            {
                CommonRequest parameters = new CommonRequest();
                parameters.sessionId = sessionId;
                parameters.clientId = clientId;
                var Data = WebClientConfig.ManageAPI("getClientMobileNo", parameters);
                var mobileObj = JsonConvert.DeserializeObject<ClientMobile>(Data);

                if (mobileObj == null) {
                    var error = JsonConvert.DeserializeObject<ErrorResponse>(Data);
                    return Content(HttpStatusCode.BadRequest, error);
                }
                return Content(HttpStatusCode.OK, new { mobileObj.mobile, errorCode = 0 });

            }
            catch (Exception e)
            {

                return Content(HttpStatusCode.InternalServerError, new{
                    errorMessage = HelperClass.GetEnumDescription(ErrorMessages.error_occured),
                    errorCode = ErrorMessages.error_occured
                });
            }
        }


    }
}