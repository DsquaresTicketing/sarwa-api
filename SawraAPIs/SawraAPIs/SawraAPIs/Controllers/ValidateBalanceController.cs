﻿using Newtonsoft.Json;
using Sarwa.App_Start;
using Sarwa.Models.requestObjects;
using Sarwa.Models.responseObjects;
using SawraAPIs.Enums;
using SawraAPIs.FilterAction;
using SawraAPIs.HelperClassess;
using SawraAPIs.Models.requestObjects;
using SawraAPIs.Models.responseObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;

namespace Sarwa.Controllers
{
   // [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class ValidateBalanceController : ApiController
    {
        // GET: ValidateBalance
        [HttpPost]
        [SessionRefresherFilter]
        public IHttpActionResult post([FromBody]Loan validBalance,string sessionId)
        {
            try
            {
                validBalance.sessionId = sessionId;
                var Data = WebClientConfig.ManageAPI("validateBalance", validBalance);
                var isValid = JsonConvert.DeserializeObject<ValidateBalance>(Data);

                if (isValid == null)
                {
                    var error = JsonConvert.DeserializeObject<ErrorResponse>(Data);
                    return Content(HttpStatusCode.BadRequest, error);
                }
                return Content(HttpStatusCode.OK, new { isValid.validBalance, errorCode = ErrorMessages.success });
            }

            catch (Exception e)
            {

                return Content(HttpStatusCode.InternalServerError, new
                {
                    errorMessage = HelperClass.GetEnumDescription(ErrorMessages.error_occured),
                    errorCode = ErrorMessages.error_occured
                });
            }
        }
    }
}