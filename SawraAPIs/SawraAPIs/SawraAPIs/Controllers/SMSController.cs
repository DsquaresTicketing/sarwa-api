﻿using Loyalty360.Services;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SawraAPIs.DAL;
using SawraAPIs.DBEntities;
using SawraAPIs.Enums;
using SawraAPIs.FilterAction;
using SawraAPIs.HelperClassess;

namespace SawraAPIs.Controllers
{
    [RoutePrefix("api/sms")]
    public class SMSController : ApiController
    {


        // POST api/<controller>
        [HttpGet]
        [SessionRefresherFilter]
        public IHttpActionResult SendMessage(string mobileNo, string sessionId)
        {
            try
            {
                var db = new SarwaContext();
                var otpObj = db.SMSCredentials.FirstOrDefault(c => c.isValid == true && c.expirationDate > DateTime.Now);
                Random generator = new Random();
                String msg = otpObj == null ? generator.Next(0, 999999).ToString("D6") : otpObj.code;
                 log4net.LogManager.GetLogger(GetType()).Info("before call send message");

                SMSService smsService = new SMSService(mobileNo, msg, "1");
                var messageSent = smsService.SendSMS();
                if (messageSent)
                {
                    int expiryHours = int.Parse(ConfigurationManager.AppSettings["messageExpiryHours"]);

                    db.SMSCredentials.Add(new SMSCredentials()
                    {
                        code = msg,
                        expirationDate = DateTime.Now.AddHours(expiryHours),
                        isValid = true,
                        mobileNo = mobileNo
                    });
                    if (db.SaveChanges() > 0)
                        return Content(HttpStatusCode.OK, new
                        {
                            message = HelperClass.GetEnumDescription(ErrorMessages.success),
                            errorCode = ErrorMessages.success
                        });
                }

                return Content(HttpStatusCode.BadRequest, new
                {
                    message = HelperClass.GetEnumDescription(ErrorMessages.otpMessageError),
                    errorCode = ErrorMessages.otpMessageError
                });



            }
            catch (Exception)
            {
                return Content(HttpStatusCode.InternalServerError, new
                {
                    errorMessage = HelperClass.GetEnumDescription(ErrorMessages.error_occured),
                    errorCode = ErrorMessages.error_occured
                });
            }
        }

        [HttpGet]
        [SessionRefresherFilter]
        [Route("VerifyOtp")]
        public IHttpActionResult VerifyOtp(string otp, string mobileNo, string sessionId)
        {
            try
            {
                var db = new SarwaContext();
                var otpObj = db.SMSCredentials.FirstOrDefault(c => c.isValid && c.code == otp &&
                                                         c.mobileNo == mobileNo && c.expirationDate > DateTime.Now);
                if (otpObj != null)
                {
                    otpObj.isValid = false;
                    db.Entry(otpObj).State = EntityState.Modified;
                    db.SaveChanges();
                    return Content(HttpStatusCode.OK, new
                    {
                        errorMessage = HelperClass.GetEnumDescription(ErrorMessages.success),
                        errorCode = ErrorMessages.success
                    });
                }

                return Content(HttpStatusCode.BadRequest, new
                {
                    errorMessage = HelperClass.GetEnumDescription(ErrorMessages.invalidOtp),
                    errorCode = ErrorMessages.invalidOtp
                });
            }
            catch (Exception e)
            {
                return Content(HttpStatusCode.InternalServerError, new
                {
                    errorMessage = HelperClass.GetEnumDescription(ErrorMessages.error_occured),
                    errorCode = ErrorMessages.error_occured
                });
            }

        }

    }
}