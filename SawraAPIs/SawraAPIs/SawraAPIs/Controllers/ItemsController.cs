﻿using Newtonsoft.Json;
using Sarwa.App_Start;
using Sarwa.Models;
using SawraAPIs.DAL;
using SawraAPIs.Enums;
using SawraAPIs.FilterAction;
using SawraAPIs.HelperClassess;
using SawraAPIs.Models.requestObjects;
using SawraAPIs.Models.responseObjects;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;

namespace Sarwa.Controllers
{
   
    public class ItemsController : ApiController
    {
               // GET: Items
        [HttpGet]
        [SessionRefresherFilter]
        public IHttpActionResult GetProducts(string sessionId, int subCategoryId)
        {
            try
            {
                SubcategoryVm prameters = new SubcategoryVm();
                prameters.sessionId = sessionId;
                prameters.subCategoryId = subCategoryId;
                var Data = WebClientConfig.ManageAPI("getItems", prameters);
                var products = JsonConvert.DeserializeObject<ProductsList>(Data);

                if (products == null){
                    var error = JsonConvert.DeserializeObject<ErrorResponse>(Data);
                    return Content(HttpStatusCode.BadRequest, error);
                }
                return Content(HttpStatusCode.OK, new { products.items, errorCode = ErrorMessages.success , sessionId});
            }

            catch (Exception e)
            {

                return Content(HttpStatusCode.InternalServerError, new{
                        errorMessage = HelperClass.GetEnumDescription(ErrorMessages.error_occured),
                        errorCode = ErrorMessages.error_occured
                    });
            }
        }
    }
}