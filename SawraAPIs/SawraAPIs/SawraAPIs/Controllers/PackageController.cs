﻿using Newtonsoft.Json;
using Sarwa.App_Start;
using Sarwa.Models;
using SawraAPIs.Enums;
using SawraAPIs.FilterAction;
using SawraAPIs.HelperClassess;
using SawraAPIs.Models.requestObjects;
using SawraAPIs.Models.responseObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;

namespace Sarwa.Controllers
{
    //[EnableCors(origins: "*", headers: "*", methods: "*")]

    public class PackageController : ApiController
    {
        // GET: Package
        [HttpGet]
        [SessionRefresherFilter]

        public IHttpActionResult GetPackage(string sessionId, string clientId)
        {
            try
            {
                             
                CommonRequest packageRequest = new CommonRequest();
                packageRequest.sessionId = sessionId;
                packageRequest.clientId = clientId;
                var Data = WebClientConfig.ManageAPI("getPackage", packageRequest);
                var package = JsonConvert.DeserializeObject<Package>(Data);
                var error = JsonConvert.DeserializeObject<ErrorResponse>(Data);
                if (error != null &&error.errorCode == "814")
                {
                    return Content(HttpStatusCode.BadRequest, error);
                }
                return Content(HttpStatusCode.OK, new { package, errorCode = ErrorMessages.success });
            }

            catch (Exception e)
            {

                return Content(HttpStatusCode.InternalServerError, new
                {
                    errorMessage = HelperClass.GetEnumDescription(ErrorMessages.error_occured),
                    errorCode = ErrorMessages.error_occured
                });
            }
        }
    }
}