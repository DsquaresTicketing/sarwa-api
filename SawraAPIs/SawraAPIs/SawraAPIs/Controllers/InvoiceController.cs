﻿using AutoMapper;
using log4net;
using Newtonsoft.Json;
using Sarwa.App_Start;
using SawraAPIs.DAL;
using SawraAPIs.Enums;
using SawraAPIs.FilterAction;
using SawraAPIs.HelperClassess;
using SawraAPIs.Models.requestObjects;
using SawraAPIs.Models.responseObjects;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;

namespace Sarwa.Controllers
{     [RoutePrefix("api/invoice")]
    public class InvoiceController : ApiController
    {   
        // SubmitInvoice
        [HttpPost]
        [SessionRefresherFilter]
        public IHttpActionResult SubmitInvoice([FromBody] Invoice invoice, string sessionId)
        {
            try
            {
                invoice.sessionId = sessionId;
                
                var Data = WebClientConfig.ManageAPI("submitInvoice", invoice);
                var invoiceNo = JsonConvert.DeserializeObject<NewInvoiceRespose>(Data);
                if (invoiceNo == null || invoiceNo.invoiceNo == 0)
                {
                    var error = JsonConvert.DeserializeObject<ErrorResponse>(Data);
                    return Content(HttpStatusCode.BadRequest, error);
                }
                //todo api should return seqno and we looping for all items to set seqno for each 
                var Data2 = WebClientConfig.ManageAPI("GetInvoiceDetails",
                    new { sessionId, invoice.clientId,
                    invoiceNo = invoiceNo.invoiceNo });
                var invoiceDetails = JsonConvert.DeserializeObject<InvoiceDetails>(Data2);
                if (invoiceDetails == null)
                    return Content(HttpStatusCode.BadRequest, new
                    {
                        errorMessage = HelperClass.GetEnumDescription(ErrorMessages.error_occured),
                        errorCode = ErrorMessages.error_occured
                    });
                var configuration = new MapperConfiguration(c=>
                {
                    c.CreateMap<ItemDetails, SawraAPIs.DBEntities.InvoiceDetails>()
                        .ForMember(k=> k.ItemId, o => o.MapFrom(s => s.itemId))
                        .ForMember(d => d.DownPayment, o => o.MapFrom(s => s.downPayment))
                        .ForMember(d => d.Price, o => o.MapFrom(s => s.price))
                        .ForMember(d => d.Tenor, o => o.MapFrom(s => s.tenor))
                        .ForMember(d => d.InstallmentSeqNo, o => o.MapFrom(s => s.installmentSeqNo))
                        .ForMember(d => d.InvoiceNo, o => o.UseValue(invoiceNo.invoiceNo))
                        .ForAllOtherMembers(cd => cd.Ignore());
                });
                var mapper = new Mapper(configuration);
                
                var invoiceDetail2 =
                        mapper.DefaultContext.Mapper.Map<List<ItemDetails>,
                            List<SawraAPIs.DBEntities.InvoiceDetails>>(invoiceDetails.items);
                configuration.AssertConfigurationIsValid();
                ////////////
                //var invoiceDetail =
                //    Mapper.Map<List<Item>, List<SawraAPIs.DBEntities.InvoiceDetails>>(invoice.items);
                var db = new SarwaContext();
                db.Invoices.Add(new SawraAPIs.DBEntities.Invoice()
                {
                    ClientId = invoice.clientId,
                    CreationDate = DateTime.Now,
                    DownPayment = invoice.downPayment,
                    Price = invoice.price,
                    InvoiceNo = invoiceNo.invoiceNo,
                    Status = (int) InvoiceStatus.Default,
                    InvoiceDetailsList = invoiceDetail2
                });
                if (db.SaveChanges() > 0 )
                    return Content(HttpStatusCode.OK, new { invoiceNo.invoiceNo , errorCode = 0 });
                else
                    return Content(HttpStatusCode.BadRequest, new
                    {
                        errorMessage = HelperClass.GetEnumDescription(ErrorMessages.InternalDbError),
                        errorCode = ErrorMessages.InternalDbError
                    });

            }
            catch (Exception e)
            {
                LogManager.GetLogger(GetType()).Info(e);

                return Content(HttpStatusCode.InternalServerError, new
                {
                    errorMessage = HelperClass.GetEnumDescription(ErrorMessages.error_occured),
                    errorCode = ErrorMessages.error_occured
                });
            }
        }

        //get all invoices
        [HttpGet]
        [SessionRefresherFilter]
        public IHttpActionResult GetInvoicesByClientId(string sessionId, string clientId)
        {
            try
            {
                CommonRequest req = new CommonRequest();
                req.sessionId = sessionId;
                req.clientId = clientId;
                var Data = WebClientConfig.ManageAPI("getRecentInvoices", req);
                var invoiceObj = JsonConvert.DeserializeObject<InvoiceSummary>(Data);
                if (invoiceObj == null)
                {
                    var error = JsonConvert.DeserializeObject<ErrorResponse>(Data);
                    return Content(HttpStatusCode.BadRequest, error);
                }
                return Content(HttpStatusCode.OK, new { invoiceObj.invoiceSummaries, errorCode = 0 });

            }
            catch (Exception e)
            {
                LogManager.GetLogger(GetType()).Info(e);

                return Content(HttpStatusCode.InternalServerError, new
                {
                    errorMessage = HelperClass.GetEnumDescription(ErrorMessages.error_occured),
                    errorCode = ErrorMessages.error_occured
                });
            }

        }

        [HttpGet]
        [SessionRefresherFilter]
        [Route("GetInvoicesDetails")]
        public IHttpActionResult GetInvoicesDetails(string sessionId, string clientId, int invoiceNumber)
        {
            try
            {
                var Data = WebClientConfig.ManageAPI("GetInvoiceDetails", new {sessionId,clientId, invoiceNo = invoiceNumber });
                var invoiceDetails = JsonConvert.DeserializeObject<InvoiceDetails>(Data);
                if (invoiceDetails == null)
                {
                    var error = JsonConvert.DeserializeObject<ErrorResponse>(Data);
                    return Content(HttpStatusCode.BadRequest, error);
                }
                return Content(HttpStatusCode.OK, new { invoiceDetails, errorCode = 0 });

            }
            catch (Exception e)
            {

                return Content(HttpStatusCode.InternalServerError, new
                {
                    errorMessage = HelperClass.GetEnumDescription(ErrorMessages.error_occured),
                    errorCode = ErrorMessages.error_occured
                });
            }
        }
        [HttpPost]
        [SessionRefresherFilter]
        [Route("RefundItem")]
        public IHttpActionResult RefundItem(string sessionId, RefundItem refundItem)
        {
            try
            {
                LogManager.GetLogger(GetType()).Info("RefundItem, SessionId : " + sessionId);
                refundItem.sessionId = sessionId;
                var Data = WebClientConfig.ManageAPI("submitRefund", refundItem);
                var refundObj = JsonConvert.DeserializeObject<RefundNo>(Data);
                if (refundObj.refundNo == null)
                {
                    var error = JsonConvert.DeserializeObject<ErrorResponse>(Data);
                    LogManager.GetLogger(GetType()).Error(error.errorMessage);
                    return Content(HttpStatusCode.BadRequest, error);
                }
                var db = new SarwaContext();
                var invoiceNumber = int.Parse(refundItem.invoiceNo);

                var items = db.InvoiceDetails.Where(c => c.InvoiceNo == invoiceNumber).ToList();
                foreach (var itemSeq in refundItem.items)
                {
                    var refundedItems = items.FirstOrDefault(c => c.InstallmentSeqNo == itemSeq.installmentSeqNo);
                    refundedItems.RefundNo = int.Parse(refundObj.refundNo);
                    refundedItems.RefundDate = DateTime.Now;
                    db.Entry(refundedItems).State = EntityState.Modified;
                }

                var invoice = db.Invoices.FirstOrDefault(c => c.InvoiceNo == invoiceNumber);
                if (items.Count > refundItem.items.Count)
                    invoice.Status = (int) InvoiceStatus.PartialRefunded;
                else
                    invoice.Status = (int)InvoiceStatus.Refunded;
                db.Entry(invoice).State = EntityState.Modified;

                if (db.SaveChanges() > 0)
                    return Content(HttpStatusCode.OK, new { refundObj.refundNo, errorCode = 0 });

                return Content(HttpStatusCode.BadRequest, new
                {
                    errorMessage = HelperClass.GetEnumDescription(ErrorMessages.InternalDbError),
                    errorCode = ErrorMessages.InternalDbError
                });

            }
            catch (Exception e)
            {
                log4net.LogManager.GetLogger(GetType()).Info(e);
                return Content(HttpStatusCode.InternalServerError, new
                {
                    errorMessage = HelperClass.GetEnumDescription(ErrorMessages.error_occured),
                    errorCode = ErrorMessages.error_occured
                });
            }
        }
    }
}