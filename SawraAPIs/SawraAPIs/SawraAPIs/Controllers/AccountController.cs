﻿using System;
using System.Configuration;
using System.Net;
using System.Web.Http;
using System.Web.Http.Cors;
using Newtonsoft.Json;
using Sarwa.App_Start;
using Sarwa.Models.BasicAPIs;
using SawraAPIs.DAL;
using SawraAPIs.DBEntities;
using SawraAPIs.Enums;
using SawraAPIs.FilterAction;
using SawraAPIs.HelperClassess;
using SawraAPIs.Models.requestObjects;
using SawraAPIs.Models.responseObjects;
using log4net;

namespace SawraAPIs.Controllers
{
    public class AccountController : ApiController
    {
        private readonly SarwaContext _db;
        public AccountController()
        {
            _db = new SarwaContext();
        }
        [HttpGet]
        public IHttpActionResult GetAccount(string authUsername,string authPassword)
        {
            try
            {
               var req = new AuthUser();
                req.authUsername = authUsername;
                req.authPassword = authPassword;
                var Data = WebClientConfig.ManageAPI<AuthUser>("Login", req);
                var reponse = JsonConvert.DeserializeObject<Session>(Data);
                if (reponse.sessionId == null)
                {
                    log4net.LogManager.GetLogger(GetType()).Info(reponse);
                    var error = JsonConvert.DeserializeObject<ErrorResponse>(Data);
                    return Content(HttpStatusCode.BadRequest, error);
                }
                var hours = double.Parse( ConfigurationManager.AppSettings["sessionExpiryTimeInHours"]);

                var user = new UserSession()
                {
                    SessionId = reponse.sessionId,
                    Username = authUsername,
                    ExpiryTime = DateTime.Now.AddHours(hours)
                };
                _db.UsersSessions.Add(user);
                if (_db.SaveChanges() > 0)
                    return Content(HttpStatusCode.OK, new { data= reponse, errorCode = ErrorMessages.success });
                else
                    return Content(HttpStatusCode.BadRequest,
                      new
                      {
                          errorCode = ErrorMessages.error_occured,
                          errorMessage = HelperClass.GetEnumDescription(ErrorMessages.error_occured),
                      });
            }
            catch (Exception e)
            {
                LogManager.GetLogger(GetType()).Info(e);

                return Content(HttpStatusCode.InternalServerError,
                    new
                    {
                        errorMessage = HelperClass.GetEnumDescription(ErrorMessages.error_occured),
                        errorCode = ErrorMessages.error_occured
                    });
            }
        }
        [HttpPost]
        [SessionRefresherFilter]
        public IHttpActionResult Logout(string sessionId)
        {
            try
            {
                var Data = WebClientConfig.ManageAPI("logout", new { sessionId });
                var islogedOut = JsonConvert.DeserializeObject<Logout>(Data);
                if (islogedOut == null)
                {
                    var error = JsonConvert.DeserializeObject<ErrorResponse>(Data);
                    return Content(HttpStatusCode.BadRequest, error);
                }
                return Content(HttpStatusCode.OK, new { islogedOut.loggedOut, errorCode = 0 });

            }
            catch (Exception e)
            {
                LogManager.GetLogger(GetType()).Info(e);

                return Content(HttpStatusCode.InternalServerError, new
                {
                    errorMessage = HelperClass.GetEnumDescription(ErrorMessages.error_occured),
                    errorCode = ErrorMessages.error_occured
                });
            }
        }
    }

}

