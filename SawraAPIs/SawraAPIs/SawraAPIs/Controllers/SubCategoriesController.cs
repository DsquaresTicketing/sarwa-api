﻿using Newtonsoft.Json;
using Sarwa.App_Start;
using Sarwa.Models;
using SawraAPIs.Enums;
using SawraAPIs.FilterAction;
using SawraAPIs.HelperClassess;
using SawraAPIs.Models.requestObjects;
using SawraAPIs.Models.responseObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;

namespace Sarwa.Controllers
{
    // [EnableCors(origins: "*", headers: "*", methods: "*")]

    public class SubCategoriesController : ApiController
    {
        // GET: SubCategories
        [HttpGet]
        [SessionRefresherFilter]
        public IHttpActionResult GetSubCategories(string sessionId,int categoryId)
        {
            try
            {
                CategoryVm prameters = new CategoryVm();
                prameters.sessionId = sessionId;
                prameters.categoryId = categoryId;

                var Data = WebClientConfig.ManageAPI("getSubCategories", prameters);
                var subCategories = JsonConvert.DeserializeObject<SubCatagoriesList>(Data);
                if (subCategories == null)
                {
                    var error = JsonConvert.DeserializeObject<ErrorResponse>(Data);
                    return Content(HttpStatusCode.BadRequest, error);
                }
                return Content(HttpStatusCode.OK, new { subCategories.subCategories, errorCode = ErrorMessages.success });
            }

            catch (Exception e)
            {

                return Content(HttpStatusCode.InternalServerError, new
                {
                    errorMessage = HelperClass.GetEnumDescription(ErrorMessages.error_occured),
                    errorCode = ErrorMessages.error_occured
                });
            }
        }
    }
}