﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Web;
using SawraAPIs.Models.requestObjects;
using System.Data.Entity;
using System.Globalization;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Xml;
using Loyalty360.Services;

namespace SawraAPIs.HelperClassess
{
    public static class HelperClass
    {
      
       
        public static string GetEnumDescription(System.Enum value)
        {
            FieldInfo fi = value.GetType().GetField(value.ToString());

            DescriptionAttribute[] attributes =
                (DescriptionAttribute[])fi.GetCustomAttributes(typeof(DescriptionAttribute), false);

            if (attributes != null && attributes.Length > 0)
                return attributes[0].Description;
            else
                return value.ToString();
        }
        public static byte[] GenerateXMLSMSRequest(SubmitSMSRequest smsRequest)
        {
            // Create the xml document in a memory stream - Recommended
            MemoryStream mStream = new MemoryStream();

            //XmlTextWriter xmlWriter =
            //         new XmlTextWriter(@"C:\Employee.xml", Encoding.UTF8);
            XmlTextWriter xmlWriter = new XmlTextWriter(mStream, Encoding.UTF8);

            xmlWriter.Formatting = Formatting.Indented;
            xmlWriter.WriteStartDocument();

            xmlWriter.WriteStartElement("SubmitSMSRequest");
            xmlWriter.WriteAttributeString("xmlns", "", null, "http://www.edafa.com/web2sms/sms/model/");
            xmlWriter.WriteStartElement("AccountId");
            xmlWriter.WriteString(smsRequest.AccountId);
            xmlWriter.WriteEndElement();

            xmlWriter.WriteStartElement("Password");
            xmlWriter.WriteString(smsRequest.Password);
            xmlWriter.WriteEndElement();

            xmlWriter.WriteStartElement("SecureHash");
            xmlWriter.WriteString(smsRequest.SecureHash);
            xmlWriter.WriteEndElement();

            foreach (var item in smsRequest.SMSList)
            {
                xmlWriter.WriteStartElement("SMSList");
                xmlWriter.WriteStartElement("SenderName");
                xmlWriter.WriteString(item.SenderName);
                xmlWriter.WriteEndElement();

                xmlWriter.WriteStartElement("ReceiverMSISDN");
                xmlWriter.WriteString(item.ReceiverMSISDN);
                xmlWriter.WriteEndElement();

                xmlWriter.WriteStartElement("SMSText");
                xmlWriter.WriteString(item.SMSText);
                xmlWriter.WriteEndElement();

                xmlWriter.WriteEndElement();
            }

            xmlWriter.WriteEndElement();
            xmlWriter.WriteEndDocument();


            xmlWriter.Flush();
            xmlWriter.Close();

            return mStream.ToArray();
        }
        public static string ConvertStringToHash(string InputString, string secreatKey)
        {

            // hex decode value of the secret key.
            var keybyte = HexDecode(secreatKey);

            // Encode secreat key
            var keybytes = Encoding.UTF8.GetBytes(secreatKey);

            // Initialize the keyed hash object using the secret key as the key
            HMACSHA256 hashObject = new HMACSHA256(keybytes);

            // Encode input string to UTF-8
            var messagebytes = Encoding.UTF8.GetBytes(InputString);

            // Computes the signature by hashing the salt with the secret key as the key
            var signature = hashObject.ComputeHash(messagebytes);

            // Base 64 Encode
            var encodedSignature = Convert.ToBase64String(signature);

            // URLEncode
            return BitConverter.ToString(signature).Replace("-", "").ToUpper();

        }
        private static byte[] HexDecode(string hex)
        {
            var bytes = new byte[hex.Length / 2];
            for (int i = 0; i < bytes.Length; i++)
            {
                // return result of converting decimal to hex
                bytes[i] = byte.Parse(hex.Substring(i * 2, 2), NumberStyles.HexNumber);
            }
            return bytes;
        }
    }

    public class SubmitSMSRequest
    {
        public string AccountId { get; set; }
        public string Password { get; set; }
        public string SecureHash { get; set; }
        public List<SMSList> SMSList { get; set; }
    }

}