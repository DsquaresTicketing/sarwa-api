﻿using System.Web.Http.ExceptionHandling;

namespace SawraAPIs.ErrorHandlers
{
    public class UnhandledExceptionLogger : ExceptionLogger
    {
        public override void Log(ExceptionLoggerContext context)
        {
            var log = context.Exception.ToString();
            var request = context.Request.RequestUri.AbsoluteUri;
            log4net.LogManager.GetLogger(GetType()).Error("Request: " + request + " " + "Error: " + log); 
        }
    }
}