﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SawraAPIs.Models.responseObjects
{
    public class ItemDetails
    {
        public int? itemId { get; set; }
        public string name { get; set; }
        public string category { get; set; }
        public string subCategory { get; set; }
        public string brand { get; set; }
        public decimal? price { get; set; }
        public decimal? downPayment { get; set; }
        public int? installmentSeqNo { get; set; }
        public int? tenor { get; set; }
    }
}