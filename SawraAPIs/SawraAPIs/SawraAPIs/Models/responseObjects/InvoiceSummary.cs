﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SawraAPIs.Models.requestObjects;

namespace SawraAPIs.Models.responseObjects
{
    public class InvoiceSummary
    {
        public List<InvoiceHistory> invoiceSummaries { get; set; }
    }
}