﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sarwa.Models
{
    public class Category
    {
        public string Tenor { get; set; }
        public long ID { get; set; }
        public int Code { get; set; }
        public string Name { get; set; }
        public string NameAr { get; set; }
    }

}