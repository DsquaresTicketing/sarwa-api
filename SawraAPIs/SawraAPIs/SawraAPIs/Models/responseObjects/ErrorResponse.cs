﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SawraAPIs.Models.responseObjects
{
    public class ErrorResponse
    {
        public string errorCode { get; set; }
        public string errorMessage { get; set; }
    }
}