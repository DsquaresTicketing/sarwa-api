﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SawraAPIs.Models.requestObjects
{
    public class RefundItem : CommonRequest
    {
        public string invoiceNo { get; set; }
        public List<InstallmentSeq> items { get; set; }
    }

    public class InstallmentSeq
    {
        public int installmentSeqNo { get; set; }
    }
}