﻿using Sarwa.Models.BasicAPIs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sarwa.Models
{
    public class SubCategoryRequestObject
    {
        public string SessionId { get; set; }
        public int categoryId { get; set; }
    }
}