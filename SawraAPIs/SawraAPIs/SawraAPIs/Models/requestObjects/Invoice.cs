﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SawraAPIs.Models.requestObjects
{
    public class Invoice : CommonRequest
    {
        public decimal price { get; set; }
        public decimal downPayment { get; set; }
        public List<Item> items { get; set; }

    }
}