﻿using Sarwa.Models.BasicAPIs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sarwa.Models
{
    public class ProductsRequestObject
    {
        public string SessionId { get; set; }
        public int subCategoryId { get; set; }
    }
}