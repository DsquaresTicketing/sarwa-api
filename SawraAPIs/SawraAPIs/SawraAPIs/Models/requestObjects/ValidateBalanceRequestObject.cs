﻿using SawraAPIs.Models.requestObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sarwa.Models.requestObjects
{
    public class ValidateBalanceRequest
    {
        public string SessionId { get; set; }
        public string ClientId { get; set; }
        public decimal loanAmount { get; set; }

    }
}