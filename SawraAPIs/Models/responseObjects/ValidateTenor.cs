﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SawraAPIs.Models.responseObjects
{
    public class ValidateTenor
    {
        public bool validTenor { get; set; }
        public string maxTenor { get; set; }
    }
}