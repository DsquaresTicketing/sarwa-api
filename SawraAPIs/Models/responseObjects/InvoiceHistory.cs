﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SawraAPIs.Models.responseObjects
{
    public class InvoiceHistory
    {
        public decimal price { get; set; }
        public decimal payment { get; set; }
        public long invoiceNo { get; set; }
        public DateTime date { get; set; }
    }
}