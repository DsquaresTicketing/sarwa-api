﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SawraAPIs.Models.requestObjects;

namespace SawraAPIs.Models.responseObjects
{
    public class InvoiceDetails 
    {
        public decimal? price { get; set; }
        public decimal? downPayment { get; set; }
        public long invoiceNo { get; set; }
        public DateTime date { get; set; }
        public string client { get; set; }
        public string merchant { get; set; }
        public string merchantBranch { get; set; }

        public List<ItemDetails> items { get; set; }
    }
}