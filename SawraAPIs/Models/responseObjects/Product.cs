﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sarwa.Models
{
    public class Product
    {
        public long ID { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string NameAr { get; set; }
    }
}