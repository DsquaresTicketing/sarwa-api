﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SawraAPIs.Models.responseObjects
{
    public class RefundDetails
    {
        public long invoiceNo { get; set; }
        public string invoiceDate { get; set; }
        public string refundNo { get; set; }
        public string refundDate { get; set; }
        public string client { get; set; }
        public string merchant { get; set; }
        public string merchantBranch { get; set; }
        public List<ItemDetails> items { get; set; }
    }
}