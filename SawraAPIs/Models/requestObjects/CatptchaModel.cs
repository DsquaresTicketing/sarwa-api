﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SawraAPIs.Models.requestObjects
{
    public class CatptchaModel
    {
        public string CaptchaId { get; set; }
        public string UserEnteredCaptchaCode { get; set; }
    }
}