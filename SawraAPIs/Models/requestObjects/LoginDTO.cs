﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SawraAPIs.Models.requestObjects
{
    public class LoginDTO
    {
        public string AuthUsername { get; set; }
        public string AuthPassword { get; set; }
    }
}