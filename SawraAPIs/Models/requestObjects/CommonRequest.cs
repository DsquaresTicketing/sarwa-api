﻿using Sarwa.Models.BasicAPIs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SawraAPIs.Models.requestObjects
{
    public class CommonRequest
    {
        public string sessionId { get; set; }

        public string clientId { get; set; }
    }
}