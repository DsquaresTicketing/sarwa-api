﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SawraAPIs.Models.requestObjects
{

    public class RefundItemWithOTP : RefundItem
    {
        public string otp { get; set; }
    }
}