﻿using SawraAPIs.Models.requestObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sarwa.Models.requestObjects
{
    public class Loan : CommonRequest
    {
        public decimal loanAmount { get; set; }
        public int Tenor { get; set; }
        public int Category { get; set; }

    }
    public class ValidateTenorRequest
    {
        public string sessionId { get; set; }
        public string tenor { get; set; }
        public string categoryId { get; set; }


    }
}