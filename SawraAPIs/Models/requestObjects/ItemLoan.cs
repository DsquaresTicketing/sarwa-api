﻿using Sarwa.Models.requestObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SawraAPIs.Models.requestObjects
{
    public class ItemLoan : CommonRequest
    {
        public decimal loanAmount { get; set; }
        public int tenor { get; set; }
    }
}