﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SawraAPIs.Models.requestObjects
{
    public class RefundItem : CommonRequest
    {
        public long invoiceNo { get; set; }
        public List<InstallmentSeq> items { get; set; }
    }

    public class InstallmentSeq
    {
        public long installmentSeqNo { get; set; }
    }
}