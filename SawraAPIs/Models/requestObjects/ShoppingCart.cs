﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SawraAPIs.Models.requestObjects
{
    public class ShoppingCart
    {
        public int itemId { get; set; }
        public int price { get; set; }
        public int downPayment { get; set; }
        public int tenor { get; set; }
        public bool isBonus { get; set; }


    }

}