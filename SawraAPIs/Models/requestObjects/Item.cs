﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SawraAPIs.Models.requestObjects
{
    public class Item
    {
        public int itemId { get; set; }
        public decimal price { get; set; }
        public decimal downPayment { get; set; }
        public int tenor { get; set; }
        public bool isBonus { get; set; }


    }

}