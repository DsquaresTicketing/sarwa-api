﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SawraAPIs.Models.requestObjects
{
    public class SubmitedInvoiceRequestObject
    {
        public string SessionId { get; set; }
        public string clientId { get; set; }
        public decimal price { get; set; }
        public decimal downPayment { get; set; }
        public List<ShoppingCart> items { get; set; }

    }
}