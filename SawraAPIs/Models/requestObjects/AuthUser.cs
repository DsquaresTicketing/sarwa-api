﻿
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace SawraAPIs.Models.requestObjects
{
    public class AuthUser
    {
        public string accountId {
            get {
                return ConfigurationManager.AppSettings["SarwaAccountId"].ToString();
            }
            set
            {
                ConfigurationManager.AppSettings["SarwaAccountId"].ToString();
            }
                }
        public string sourceUsername
        {
            get
            {
                return ConfigurationManager.AppSettings["sourceUsername"].ToString();
            }
            set
            {
                ConfigurationManager.AppSettings["sourceUsername"].ToString();
            }
        }
        public string sourcePassword
        {
            get
            {
                return ConfigurationManager.AppSettings["sourcePassword"].ToString();
            }
            set
            {
                ConfigurationManager.AppSettings["sourcePassword"].ToString();
            }
        }
        public string authUsername { get; set; }
        public string authPassword { get; set; }

    }
}