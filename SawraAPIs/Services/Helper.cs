﻿using Newtonsoft.Json;
using Sarwa.App_Start;
using SawraAPIs.Models.requestObjects;
using SawraAPIs.Models.responseObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SawraAPIs.Services
{
    public class Helper
    {
        public string GetClientMobileNumber(string sessionID, string clientID)
        {
            CommonRequest parameters = new CommonRequest();
            parameters.sessionId = sessionID;
            parameters.clientId = clientID;
            var Data = WebClientConfig.ManageAPI("getClientMobileNo", parameters);
            var mobileObj = JsonConvert.DeserializeObject<ClientMobile>(Data);
            if (mobileObj == null)
            { return string.Empty; }
            return mobileObj.mobile;
        }
    }
}