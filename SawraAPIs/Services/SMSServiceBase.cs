﻿
using System.Collections.Generic;
using System.Configuration;

namespace Loyalty360.Services
{
    public abstract class SMSServiceBase
    {
        public SMSServiceBase()
        {
            Sender = ConfigurationManager.AppSettings["SMS:ConfirmationSender"];
        }
        public SMSServiceBase(string to, string msg, string unicode)
        {
            To = to;
            Msg = msg;
            Unicode = unicode;

          
                Sender = ConfigurationManager.AppSettings["SMS:ConfirmationSender"];
           
            //    Sender = ConfigurationManager.AppSettings["SMS:RequiresAnswerSender"];
            
        }
        public SMSServiceBase(string to, string unicode)
        {
            To = to;
            Unicode = unicode;

            
                Sender = ConfigurationManager.AppSettings["SMS:ConfirmationSender"];
            
            //    Sender = ConfigurationManager.AppSettings["SMS:RequiresAnswerSender"];
         
        }
        public abstract bool SendSMS(string MessageID = null);
        public abstract void SendSMS(Dictionary<string, string> variables = null);
        public abstract void SendSMS(string msg, Dictionary<string, string> variables);

        // public int Priority { get; set; }
        public string To { get; set; }
        public string Msg { get; set; }
        public string Unicode { get; set; }
        public bool IsSendSMS { get; set; }
        public string Sender { get; set; }
        //    public SMSType Type
        //    {
        //        set
        //        {
        //            if (value == SMSType.Confirmation)
        //            {
        //                Sender = ConfigurationManager.AppSettings["SMS:ConfirmationSender"];
        //            }
        //            else
        //            {
        //                Sender = ConfigurationManager.AppSettings["SMS:RequiresAnswerSender"];
        //            }
        //        }
        //    }
        //}

        //public enum SMSType
        //{
        //    Confirmation,
        //    RequiresAnswer
        //}
    }
}