﻿

using log4net;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Net;
using System.Text;
using System.Web;
using SawraAPIs.HelperClassess;

namespace Loyalty360.Services
{
    public class SMSService : SMSServiceBase
    {
        public SMSService(string to, string msg, string unicode)
            : base(to, msg, unicode)
        {
        }
        public SMSService(string to, string unicode)
            : base(to, unicode)
        {
        }
        public SMSService()
            : base()
        {
        }

        override public void SendSMS( Dictionary<string, string> variables)
        {
            string langStr = !string.IsNullOrEmpty(ConfigurationManager.AppSettings["VerificationSmsLanguage"]) ? ConfigurationManager.AppSettings["VerificationSmsLanguage"].Trim() : "2" ;
          //  SmsMessageFormatter formatter = new SmsMessageFormatter(variables);
                SendSMS();
               
           
        }

        override public void SendSMS(string msg, Dictionary<string, string> variables)
        {
           // SmsMessageFormatter formatter = new SmsMessageFormatter(variables);
           
            SendSMS();
        }




        override public bool SendSMS( string MessageID = null)
        {

            LogManager.GetLogger(GetType()).Info("SendSMS");
            string smsUrl = ConfigurationManager.AppSettings["SMSUrlVFCloud"];
            string smsUser = ConfigurationManager.AppSettings["SMSUserVFCloud"];
            string smsPass = ConfigurationManager.AppSettings["SMSPasswordVFCloud"];
            var countryCode = ConfigurationManager.AppSettings["countryCode"];
            var reciever = To;
            if (!To.StartsWith(countryCode) && !To.StartsWith(countryCode.Replace("+", "")))
            {
                reciever = countryCode + To;
            }

            var sms = new SMSList()
            {
                ReceiverMSISDN = reciever,
                SenderName = Sender,
                SMSText = Msg
            };
            List<SMSList> smsList = new List<SMSList>() { sms };



            if (string.IsNullOrEmpty(smsUrl) && string.IsNullOrEmpty(smsPass) && string.IsNullOrEmpty(smsUser))
                return false;
            try
            {
                string url = smsUrl;

               

                var credCache = new CredentialCache();
                credCache.Add(new Uri(url), "Basic", new NetworkCredential(smsUser, smsPass));

                var http = (HttpWebRequest)WebRequest.Create(url);
                http.Method = "POST";
                //http.Credentials = credCache;
                http.ContentType = "application/xml";
                //http.KeepAlive = false;
                http.Timeout = 15000;

                StringBuilder data = new StringBuilder();
                var accountId = ConfigurationManager.AppSettings["AccountId"];
                data.AppendFormat("AccountId={0}&Password={1}", accountId, smsPass);


                foreach (var item in smsList)
                {
                    data.AppendFormat("&SenderName={0}", item.SenderName);
                    var to = item.ReceiverMSISDN;
                    data.AppendFormat("&ReceiverMSISDN={0}", to);
                    data.AppendFormat("&SMSText={0}", item.SMSText);
                }
                var key = ConfigurationManager.AppSettings["SecretKey"] != null ? ConfigurationManager.AppSettings["SecretKey"].ToString() : "";

                var hashedData = HelperClass.ConvertStringToHash(data.ToString(), key);
                var submitSMSRequest = new SubmitSMSRequest()
                {
                    AccountId = accountId,
                    Password = smsPass,
                    SecureHash = HttpUtility.UrlEncode(hashedData),
                    SMSList = smsList

                };

                ServicePointManager.Expect100Continue = true;
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
                byte[] dataByte = HelperClass.GenerateXMLSMSRequest(submitSMSRequest);

                // Set the content length in the request headers  
                http.ContentLength = dataByte.Length;

                using (var POSTstream = http.GetRequestStream())
                {
                    POSTstream.Write(dataByte, 0, dataByte.Length);
                }


                //Get response from server
                using (HttpWebResponse POSTResponse = http.GetResponse() as HttpWebResponse)
                {
                    StreamReader reader = new StreamReader(POSTResponse.GetResponseStream(), Encoding.UTF8);
                    var response = reader.ReadToEnd().ToString();
                    LogManager.GetLogger(GetType()).Info(response);

                    if (response.ToLower().Contains("success"))
                        return true;
                    else
                        return false;
                }
            }
            catch (Exception ex)
            {
                LogManager.GetLogger(GetType()).Error("request failed: SendSMS", ex);
                return false;
            }

        }



        
    }

    public class SMSList
    {
        public SMSList()
        {
        }

        public string ReceiverMSISDN { get; set; }
        public string SenderName { get; set; }
        public string SMSText { get; set; }
    }
}