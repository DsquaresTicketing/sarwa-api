﻿using Newtonsoft.Json;
using Sarwa.App_Start;
using Sarwa.Models.BasicAPIs;
using SawraAPIs.DAL;
using SawraAPIs.DBEntities;
using SawraAPIs.Models.requestObjects;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace SawraAPIs.HelperClassess
{
    public class SessionChecker
    {
        internal string CheckSessionExpiry(string sessionId, User userData)
        {
            //isAuthorized = false;
            using (SarwaContext _db = new SarwaContext())
            {
               var session =  _db.UsersSessions.FirstOrDefault(c => c.SessionId == sessionId);
                if (session == null ||session.ExpiryTime < DateTime.Now || session.IsExpired )
                {
                    if (session != null)
                    {
                        session.IsExpired = true;
                        _db.Entry(session).State = EntityState.Modified;
                        _db.SaveChanges();
                    }
                    var req = new AuthUser()
                    {
                        authUsername = userData.Username,
                        authPassword = userData.Password
                    };
                    var Data = WebClientConfig.ManageAPI<AuthUser>("Login", req);
                    var newSessionId = JsonConvert.DeserializeObject<Session>(Data);
                    //isAuthorized = newSessionId.SessionId != null;
                    var hours = double.Parse(ConfigurationManager.AppSettings["sessionExpiryTimeInHours"]);

                    var user = new UserSession()
                    {
                        SessionId = newSessionId.sessionId,
                        Username = userData.Username,
                        ExpiryTime = DateTime.Now.AddHours(hours)
                    };
                    _db.UsersSessions.Add(user);
                    if(_db.SaveChanges() > 0 )
                    {
                        return newSessionId.sessionId;
                    }
                    return null;
                }
                else {
                    //isAuthorized = true;
                    return sessionId; }
            }
               
        }
    }
}