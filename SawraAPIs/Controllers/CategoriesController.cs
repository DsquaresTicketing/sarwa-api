﻿using Newtonsoft.Json;
using Sarwa.App_Start;
using Sarwa.Models;
using Sarwa.Models.BasicAPIs;
using SawraAPIs.Enums;
using SawraAPIs.FilterAction;
using SawraAPIs.HelperClassess;
using SawraAPIs.Models.requestObjects;
using SawraAPIs.Models.responseObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace Sarwa.Controllers
{
    
    public class CategoriesController : ApiController
    {
        [HttpGet]
        [SessionRefresherFilter]
        public IHttpActionResult GetCategories( string sessionId)
        {
            try
            {
               
                Session prameters = new Session();
                prameters.sessionId = sessionId;
                var Data = WebClientConfig.ManageAPI("getCategories", prameters);
                var categoryObj = JsonConvert.DeserializeObject<CategoriesList>(Data);
                if (categoryObj.Categories == null) {
                     var error = JsonConvert.DeserializeObject<ErrorResponse>(Data);
                    return Content(HttpStatusCode.BadRequest, error);
                }

                return Content(HttpStatusCode.OK, new { categoryObj.Categories, errorCode = ErrorMessages.success });
                
            }
            catch (Exception e)
            {
                return Content(HttpStatusCode.InternalServerError, new {
                    errorMessage = HelperClass.GetEnumDescription( ErrorMessages.error_occured)
                    , errorCode = ErrorMessages.error_occured });
            }


        }


    }
}