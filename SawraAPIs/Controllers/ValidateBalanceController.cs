﻿using Newtonsoft.Json;
using Sarwa.App_Start;
using Sarwa.Models.requestObjects;
using Sarwa.Models.responseObjects;
using SawraAPIs.Enums;
using SawraAPIs.FilterAction;
using SawraAPIs.HelperClassess;
using SawraAPIs.Models.requestObjects;
using SawraAPIs.Models.responseObjects;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;

namespace Sarwa.Controllers
{
    // [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class ValidateBalanceController : ApiController
    {
        // GET: ValidateBalance
        [HttpPost]
        [SessionRefresherFilter]
        public IHttpActionResult post([FromBody]Loan validBalance, string sessionId)
        {
            try
            {
                validBalance.sessionId = sessionId;
                log4net.LogManager.GetLogger(GetType()).Info(string.Format("validate tenor request is {0} {1} ,{2} ", validBalance.Tenor, validBalance.Category, sessionId));

                var validateTenorData = ValidateTenor(validBalance.Tenor, validBalance.Category, sessionId);
                log4net.LogManager.GetLogger(GetType()).Info(string.Format("validate tenor response is{0} ", validateTenorData));

                if (!string.IsNullOrEmpty(validateTenorData))
                {
                    var validateTenorResponse = JsonConvert.DeserializeObject<ValidateTenor>(validateTenorData);
                    if (!validateTenorResponse.validTenor)
                    {
                        return Content(HttpStatusCode.OK, new { validateTenorResponse.validTenor, validateTenorResponse.maxTenor, errorCode = ErrorMessages.success });
                    }
                }

                var Data = WebClientConfig.ManageAPI("validateBalance", validBalance);

                log4net.LogManager.GetLogger(GetType()).Info(string.Format("validate balance response is {0} with loan item sent {1} ", Data, validBalance.loanAmount));

                var isValid = JsonConvert.DeserializeObject<ValidateBalance>(Data);

                if (isValid == null)
                {
                    var error = JsonConvert.DeserializeObject<ErrorResponse>(Data);
                    return Content(HttpStatusCode.BadRequest, error);
                }
                return Content(HttpStatusCode.OK, new { isValid.validBalance, validTenor = true, errorCode = ErrorMessages.success });
            }

            catch (Exception e)
            {
                log4net.LogManager.GetLogger(GetType()).Info(string.Format("error in validate balance {0}  ", e.Message));
                log4net.LogManager.GetLogger(GetType()).Info(e);

                return Content(HttpStatusCode.InternalServerError, new
                {
                    errorMessage = HelperClass.GetEnumDescription(ErrorMessages.error_occured),
                    errorCode = ErrorMessages.error_occured
                });
            }
        }

        public string ValidateTenor(int tenor, int categoryId, string sessionId)
        {
            try
            {
                log4net.LogManager.GetLogger(GetType()).Info("call validate tenor ");
                var requestObj = new ValidateTenorRequest
                {
                    sessionId = sessionId.ToString(),
                    categoryId = categoryId.ToString(),
                    tenor = tenor.ToString()
                };
                return WebClientConfig.ManageAPI("validateTenor", requestObj);
            }
            catch (Exception e)
            {
                log4net.LogManager.GetLogger(GetType()).Info(string.Format("error in validate tenor  {0}  ", e.Message));
                log4net.LogManager.GetLogger(GetType()).Info(e);
                return string.Empty;
            }
        }

    }
}