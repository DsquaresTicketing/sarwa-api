﻿using Loyalty360.Services;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SawraAPIs.DAL;
using SawraAPIs.DBEntities;
using SawraAPIs.Enums;
using SawraAPIs.FilterAction;
using SawraAPIs.HelperClassess;
using System.Threading.Tasks;
using SawraAPIs.Models.requestObjects;
using Sarwa.App_Start;
using Newtonsoft.Json;
using SawraAPIs.Models.responseObjects;
using SawraAPIs.Services;

namespace SawraAPIs.Controllers
{
    [RoutePrefix("api/sms")]
    public class SMSController : ApiController
    {
        private Helper _helper;

        public SMSController()
        {
            _helper = new Helper();
        }
        // POST api/<controller>
        [HttpGet]
        [SessionRefresherFilter]
        public IHttpActionResult SendMessage(string clientID, string sessionId, int type, decimal amount)
        {
            try
            {
                var mobileNumber = _helper.GetClientMobileNumber(sessionId, clientID);
                if (string.IsNullOrEmpty(mobileNumber))
                {
                    log4net.LogManager.GetLogger(GetType()).Info(string.Format("client: {0} mobile number is empty request with session id: {1}", clientID, sessionId));
                    return BadRequest();
                }
                var db = new SarwaContext();
                var otpObj = db.SMSCredentials.FirstOrDefault(c => c.isValid == true && c.expirationDate > DateTime.Now && c.mobileNo == mobileNumber);
                Random generator = new Random();
                var msg = GenerateMessgaeBody(type, amount);
                var code = otpObj == null ? generator.Next(0, 999999).ToString("D6") : otpObj.code;
                msg = msg.Replace("{Code}", code);
                log4net.LogManager.GetLogger(GetType()).Info("before call send message");



                SMSService smsService = new SMSService(mobileNumber, msg, "1");
                var messageSent = smsService.SendSMS();
                log4net.LogManager.GetLogger(GetType()).Info("message sent to mobile number:  " + mobileNumber);

                if (messageSent && otpObj == null)
                {

                    int expiryHours = int.Parse(ConfigurationManager.AppSettings["messageExpirySeconds"]);

                    db.SMSCredentials.Add(new SMSCredentials()
                    {
                        code = code,
                        expirationDate = DateTime.Now.AddHours(expiryHours),
                        isValid = true,
                        mobileNo = mobileNumber
                    });
                    if (db.SaveChanges() > 0)
                        return Content(HttpStatusCode.OK, new
                        {
                            message = HelperClass.GetEnumDescription(ErrorMessages.success),
                            errorCode = ErrorMessages.success
                        });
                }

                return Content(HttpStatusCode.BadRequest, new
                {
                    message = HelperClass.GetEnumDescription(ErrorMessages.otpMessageError),
                    errorCode = ErrorMessages.otpMessageError
                });



            }
            catch (Exception ex)
            {
                log4net.LogManager.GetLogger(GetType()).Info(string.Format("error in sending sms to client id  {0} , {1}", clientID, ex.Message));

                return Content(HttpStatusCode.InternalServerError, new
                {
                    errorMessage = HelperClass.GetEnumDescription(ErrorMessages.error_occured),
                    errorCode = ErrorMessages.error_occured
                });
            }
        }
        private string GenerateMessgaeBody(int type, decimal amount)
        {
            //submit invoice
            if (type == 1)
            {
                var msg = "  لتأكيد عملية تقسيط مشتريات بقيمة {Amount} ، برجاء إستخدام هذا الكود{Code} . إستخدامك لكود التأكيد يعد موافقة على الشروط والأحكام الموجودة في هذا الرابط https://tiny.cc/3ufdfz";

                return msg.Replace("{Amount}", amount.ToString());
            }
            else
            {
                var refundMsg = "  لتأكيد عملية الاسترجاع ، برجاء إستخدام هذا الكود{Code} . إستخدامك لكود التأكيد يعد موافقة على الشروط والأحكام الموجودة في هذا الرابط https://tiny.cc/3ufdfz";
                return refundMsg;
            }
        }

        [HttpGet]
        [SessionRefresherFilter]
        [Route("VerifyOtp")]
        public IHttpActionResult VerifyOtp(string otp, string mobileNo, string sessionId)
        {
            try
            {
                var db = new SarwaContext();
                var otpObj = db.SMSCredentials.FirstOrDefault(c => c.isValid && c.code == otp &&
                                                         c.mobileNo == mobileNo && c.expirationDate > DateTime.Now);
                if (otpObj != null)
                {
                    otpObj.isValid = false;
                    otpObj.expirationDate = DateTime.Now;
                    db.Entry(otpObj).State = EntityState.Modified;

                    db.SaveChanges();
                    return Content(HttpStatusCode.OK, new
                    {
                        errorMessage = HelperClass.GetEnumDescription(ErrorMessages.success),
                        errorCode = ErrorMessages.success
                    });
                }

                return Content(HttpStatusCode.BadRequest, new
                {
                    errorMessage = HelperClass.GetEnumDescription(ErrorMessages.invalidOtp),
                    errorCode = ErrorMessages.invalidOtp
                });
            }
            catch (Exception e)
            {
                return Content(HttpStatusCode.InternalServerError, new
                {
                    errorMessage = HelperClass.GetEnumDescription(ErrorMessages.error_occured),
                    errorCode = ErrorMessages.error_occured
                });
            }

        }

    }
}