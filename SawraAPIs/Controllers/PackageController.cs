﻿using Newtonsoft.Json;
using Sarwa.App_Start;
using Sarwa.Models;
using SawraAPIs.Enums;
using SawraAPIs.FilterAction;
using SawraAPIs.HelperClassess;
using SawraAPIs.Models.requestObjects;
using SawraAPIs.Models.responseObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;

namespace Sarwa.Controllers
{
    //[EnableCors(origins: "*", headers: "*", methods: "*")]

    public class PackageController : ApiController
    {
        // GET: Package
        [HttpGet]
        [SessionRefresherFilter]

        public IHttpActionResult GetPackage(string sessionId, string clientId)
        {
            try
            {

                CommonRequest packageRequest = new CommonRequest();
                packageRequest.sessionId = sessionId;
                packageRequest.clientId = clientId;
                var Data = WebClientConfig.ManageAPI("getPackage", packageRequest);
                var package = JsonConvert.DeserializeObject<Package>(Data);
                var error = JsonConvert.DeserializeObject<ErrorResponse>(Data);
                if (error.errorCode != null)
                {
                    if (int.Parse(error.errorCode) == (int)PackageErrorsEnum.Availablebalanceinsufficient)
                    {
                        error.errorMessage = "الاحد الاتمانى المتاح غير كافى";
                        return Content(HttpStatusCode.BadRequest, error);
                    }
                    if (int.Parse(error.errorCode) == (int)PackageErrorsEnum.Customeraccountisdeactivatedorsuspended)
                    {
                        error.errorMessage = "حساب العميل قد يكون غير مفعل او موقوف يرجي توجيه العيل للاتصال ب 16177";
                        return Content(HttpStatusCode.BadRequest, error);
                    }
                    if (int.Parse(error.errorCode) == (int)PackageErrorsEnum.CustomerDosntExist)
                    {
                        error.errorMessage = "هذا العميل غير مسجل";
                        return Content(HttpStatusCode.BadRequest, error);
                    }
                    if (int.Parse(error.errorCode) == (int)PackageErrorsEnum.InvalidSession)
                        return Content(HttpStatusCode.BadRequest, error);
                }
                return Content(HttpStatusCode.OK, new { package, errorCode = ErrorMessages.success });
            }

            catch (Exception e)
            {

                return Content(HttpStatusCode.InternalServerError, new
                {
                    errorMessage = HelperClass.GetEnumDescription(ErrorMessages.error_occured),
                    errorCode = ErrorMessages.error_occured
                });
            }
        }
    }
}