﻿using AutoMapper;
using log4net;
using Newtonsoft.Json;
using Sarwa.App_Start;
using SawraAPIs.DAL;
using SawraAPIs.Enums;
using SawraAPIs.FilterAction;
using SawraAPIs.HelperClassess;
using SawraAPIs.Models.requestObjects;
using SawraAPIs.Models.responseObjects;
using SawraAPIs.Services;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;

namespace Sarwa.Controllers
{
    [RoutePrefix("api/invoice")]
    public class InvoiceController : ApiController
    {
        private Helper _helper;

        public InvoiceController()
        {
            _helper = new Helper();
        }
        // SubmitInvoice
        [HttpPost]
        [SessionRefresherFilter]
        public IHttpActionResult SubmitInvoice([FromBody] InvoiceWithOTP invoice, string sessionId)
        {
            try
            {
                invoice.sessionId = sessionId;
                var mobileNumber = _helper.GetClientMobileNumber(sessionId, invoice.clientId);
                var minmmunDownPayment = int.Parse(ConfigurationManager.AppSettings["MinimmunDownPayment"]);
                if (minmmunDownPayment > (invoice.price - invoice.downPayment))
                    return Content(HttpStatusCode.BadRequest, new ErrorResponse { errorCode = "", errorMessage = " الحد الادنى للمشتريات 1000 جنية مصري" });

                var db = new SarwaContext();

                if (db.SMSCredentials.Any(c => c.isValid && c.code == invoice.otp &&
                                                      c.mobileNo == mobileNumber && c.expirationDate > DateTime.Now))
                {
                    log4net.LogManager.GetLogger(GetType()).Info(string.Format("invoice for customer {0} sumitted with total amount {1} and downpayment {2}", invoice.clientId, invoice.price, invoice.downPayment));
                    var invoiceObj = (Invoice)invoice;
                    var Data = WebClientConfig.ManageAPI("submitInvoice", invoiceObj);
                    log4net.LogManager.GetLogger(GetType()).Info("response for submit invoice " + Data);

                    var newInvoiceRespose = JsonConvert.DeserializeObject<InvoiceResposee>(Data);

                    long parsedInvoiceNo;
                    if (newInvoiceRespose == null || !long.TryParse(newInvoiceRespose.invoiceNo, out parsedInvoiceNo))
                    {
                        var error = JsonConvert.DeserializeObject<ErrorResponse>(Data);
                        return Content(HttpStatusCode.BadRequest, error);
                    }
                    //todo api should return seqno and we looping for all items to set seqno for each 
                    var Data2 = WebClientConfig.ManageAPI("GetInvoiceDetails",
                        new
                        {
                            sessionId,
                            invoice.clientId,
                            invoiceNo = newInvoiceRespose.invoiceNo
                        });

                    log4net.LogManager.GetLogger(GetType()).Info("reposne from get invoice details :  " + Data2);

                    var invoiceDetails = JsonConvert.DeserializeObject<InvoiceDetails>(Data2);
                    if (invoiceDetails == null)
                        return Content(HttpStatusCode.BadRequest, new
                        {
                            errorMessage = HelperClass.GetEnumDescription(ErrorMessages.error_occured),
                            errorCode = ErrorMessages.error_occured
                        });
                    var configuration = new MapperConfiguration(c =>
                    {
                        c.CreateMap<ItemDetails, SawraAPIs.DBEntities.InvoiceDetails>()
                            .ForMember(k => k.ItemId, o => o.MapFrom(s => s.itemId))
                            .ForMember(d => d.DownPayment, o => o.MapFrom(s => s.downPayment))
                            .ForMember(d => d.Price, o => o.MapFrom(s => s.price))
                            .ForMember(d => d.Tenor, o => o.MapFrom(s => s.tenor))
                            .ForMember(d => d.InstallmentSeqNo, o => o.MapFrom(s => s.installmentSeqNo))
                            .ForMember(d => d.InvoiceNo, o => o.UseValue(long.Parse(newInvoiceRespose.invoiceNo)))
                            .ForAllOtherMembers(cd => cd.Ignore());
                    });
                    var mapper = new Mapper(configuration);

                    var invoiceDetail2 =
                            mapper.DefaultContext.Mapper.Map<List<ItemDetails>,
                                List<SawraAPIs.DBEntities.InvoiceDetails>>(invoiceDetails.items);
                    configuration.AssertConfigurationIsValid();
                    ////////////
                    //var invoiceDetail =
                    //    Mapper.Map<List<Item>, List<SawraAPIs.DBEntities.InvoiceDetails>>(invoice.items);
                    db.Invoices.Add(new SawraAPIs.DBEntities.Invoice()
                    {
                        ClientId = invoice.clientId,
                        CreationDate = DateTime.Now,
                        DownPayment = invoice.downPayment,
                        Price = invoice.price,
                        InvoiceNo = long.Parse(newInvoiceRespose.invoiceNo),
                        Status = (int)InvoiceStatus.Default,
                        InvoiceDetailsList = invoiceDetail2
                    });
                    var otpObj = db.SMSCredentials.FirstOrDefault(c => c.isValid && c.code == invoice.otp &&
                                                        c.mobileNo == mobileNumber && c.expirationDate > DateTime.Now);
                    otpObj.isValid = false;
                    otpObj.expirationDate = DateTime.Now;
                    db.Entry(otpObj).State = EntityState.Modified;

                    if (db.SaveChanges() > 0)
                        return Content(HttpStatusCode.OK, new { newInvoiceRespose.invoiceNo, errorCode = 0 });
                    else
                    {
                        return Content(HttpStatusCode.BadRequest, new
                        {
                            errorMessage = HelperClass.GetEnumDescription(ErrorMessages.InternalDbError),
                            errorCode = ErrorMessages.InternalDbError
                        });
                    }
                }
                else
                    return Content(HttpStatusCode.BadRequest, new
                    {
                        errorMessage = HelperClass.GetEnumDescription(ErrorMessages.invalidOtp),
                        errorCode = ErrorMessages.invalidOtp
                    });

            }
            catch (Exception e)
            {
                LogManager.GetLogger(GetType()).Info(e);

                return Content(HttpStatusCode.InternalServerError, new
                {
                    errorMessage = HelperClass.GetEnumDescription(ErrorMessages.error_occured),
                    errorCode = ErrorMessages.error_occured
                });
            }
        }

        //get all invoices
        [HttpGet]
        [SessionRefresherFilter]
        public IHttpActionResult GetInvoicesByClientId(string sessionId, string clientId)
        {
            try
            {
                CommonRequest req = new CommonRequest();
                req.sessionId = sessionId;
                req.clientId = clientId;
                var Data = WebClientConfig.ManageAPI("getRecentInvoices", req);
                LogManager.GetLogger(GetType()).Info("GetInvoicesByClientId request response  " + Data);

                var invoiceObj = JsonConvert.DeserializeObject<InvoiceSummary>(Data);
                if (invoiceObj == null)
                {
                    var error = JsonConvert.DeserializeObject<ErrorResponse>(Data);
                    return Content(HttpStatusCode.BadRequest, error);
                }
                return Content(HttpStatusCode.OK, new { invoiceObj.invoiceSummaries, errorCode = 0 });

            }
            catch (Exception e)
            {
                LogManager.GetLogger(GetType()).Info(e);

                return Content(HttpStatusCode.InternalServerError, new
                {
                    errorMessage = HelperClass.GetEnumDescription(ErrorMessages.error_occured),
                    errorCode = ErrorMessages.error_occured
                });
            }

        }

        [HttpGet]
        [SessionRefresherFilter]
        [Route("GetInvoicesDetails")]
        public IHttpActionResult GetInvoicesDetails(string sessionId, string clientId, long invoiceNumber)
        {
            try
            {
                log4net.LogManager.GetLogger(GetType()).Info(string.Format("call invoice details with sessionid {0}, clientid {1} ,invoicenumber {2}  :  ", sessionId, clientId, invoiceNumber));

                var Data = WebClientConfig.ManageAPI("GetInvoiceDetails", new { sessionId, clientId, invoiceNo = invoiceNumber });
                log4net.LogManager.GetLogger(GetType()).Info(string.Format("response from invoice details with sessionid {0}  :  ", Data));

                var invoiceDetails = JsonConvert.DeserializeObject<InvoiceDetails>(Data);
                if (invoiceDetails == null)
                {
                    var error = JsonConvert.DeserializeObject<ErrorResponse>(Data);
                    return Content(HttpStatusCode.BadRequest, error);
                }
                return Content(HttpStatusCode.OK, new { invoiceDetails, errorCode = 0 });

            }
            catch (Exception e)
            {
                log4net.LogManager.GetLogger(GetType()).Info(string.Format("exception from invoice details with sessionid {0}  :  ", e.Message));

                return Content(HttpStatusCode.InternalServerError, new
                {
                    errorMessage = HelperClass.GetEnumDescription(ErrorMessages.error_occured),
                    errorCode = ErrorMessages.error_occured
                });
            }
        }
        [HttpGet]
        [SessionRefresherFilter]
        [Route("GetRefundDetails")]
        public IHttpActionResult GetRefundDetails(string sessionId, string clientId, long refundNumber)
        {
            try
            {
                log4net.LogManager.GetLogger(GetType()).Info(string.Format("call refund details with sessionid {0}, clientid {1} ,refundNumber {2}  :  ", sessionId, clientId, refundNumber));

                var Data = WebClientConfig.ManageAPI("getRefundDetails", new { sessionId, clientId, refundNo = refundNumber });
                LogManager.GetLogger(GetType()).Info("refund details request response  " + Data);

                var refundDetails = JsonConvert.DeserializeObject<RefundDetails>(Data);
                if (refundDetails == null)
                {
                    var error = JsonConvert.DeserializeObject<ErrorResponse>(Data);
                    return Content(HttpStatusCode.BadRequest, error);
                }
                return Content(HttpStatusCode.OK, new { refundDetails, errorCode = 0 });

            }
            catch (Exception e)
            {

                log4net.LogManager.GetLogger(GetType()).Info(string.Format("exception from refund details with sessionid {0}  :  ", e.Message));

                return Content(HttpStatusCode.InternalServerError, new
                {
                    errorMessage = HelperClass.GetEnumDescription(ErrorMessages.error_occured),
                    errorCode = ErrorMessages.error_occured
                });
            }
        }
        [HttpPost]
        [SessionRefresherFilter]
        [Route("RefundItem")]
        public IHttpActionResult RefundItem(string sessionId, RefundItemWithOTP refundItem)
        {
            try
            {
                var mobileNumber = _helper.GetClientMobileNumber(sessionId, refundItem.clientId);

                LogManager.GetLogger(GetType()).Info("RefundItem, SessionId : " + sessionId + "invoice no: " + refundItem.invoiceNo + "client id: " + refundItem.clientId);
                refundItem.sessionId = sessionId;
                var db = new SarwaContext();
                if (db.SMSCredentials.Any(c => c.isValid && c.code == refundItem.otp &&
                                                      c.mobileNo == mobileNumber && c.expirationDate > DateTime.Now))
                {
                    var refundItemObj = (RefundItem)refundItem;

                    refundItemObj.items.ForEach(x => LogManager.GetLogger(GetType()).Info("request to refund item is with installmentSeqNo: " + x.installmentSeqNo.ToString()));

                    LogManager.GetLogger(GetType()).Info("request to refund item is with invoiceNo: " + refundItemObj.invoiceNo.ToString());

                    var Data = WebClientConfig.ManageAPI("submitRefund", refundItemObj);

                    LogManager.GetLogger(GetType()).Info("request from refuns item is : " + Data);

                    var refundObj = JsonConvert.DeserializeObject<RefundNo>(Data);
                    if (refundObj.refundNo == null)
                    {

                        var error = JsonConvert.DeserializeObject<ErrorResponse>(Data);
                        LogManager.GetLogger(GetType()).Error(error.errorMessage);
                        return Content(HttpStatusCode.BadRequest, error);
                    }
                    LogManager.GetLogger(GetType()).Info("request for refund succeded ");

                    var invoiceNumber = refundItem.invoiceNo;

                    if (db.InvoiceDetails.Any(c => c.InvoiceNo == invoiceNumber))
                    {
                        LogManager.GetLogger(GetType()).Info("invoice details found in db for invoice number :  " + invoiceNumber);

                        var items = db.InvoiceDetails.Where(c => c.InvoiceNo == invoiceNumber).ToList();
                        foreach (var itemSeq in refundItem.items)
                        {
                            var refundedItems = items.FirstOrDefault(c => c.InstallmentSeqNo == itemSeq.installmentSeqNo);
                            long refundNoParsed = 0;
                            if (long.TryParse(refundObj.refundNo, out  refundNoParsed))
                                refundedItems.RefundNo = refundNoParsed;

                            refundedItems.RefundDate = DateTime.Now;
                            db.Entry(refundedItems).State = EntityState.Modified;
                        }

                        var invoice = db.Invoices.FirstOrDefault(c => c.InvoiceNo == invoiceNumber);
                        if (items.Count > refundItem.items.Count)
                            invoice.Status = (int)InvoiceStatus.PartialRefunded;
                        else
                            invoice.Status = (int)InvoiceStatus.Refunded;
                        db.Entry(invoice).State = EntityState.Modified;
                    }
                    var otpObj = db.SMSCredentials.FirstOrDefault(c => c.isValid && c.code == refundItem.otp &&
                                                        c.mobileNo == mobileNumber && c.expirationDate > DateTime.Now);
                    otpObj.isValid = false;
                    otpObj.expirationDate = DateTime.Now;
                    db.Entry(otpObj).State = EntityState.Modified;


                    if (db.SaveChanges() > 0)
                        return Content(HttpStatusCode.OK, new { refundObj.refundNo, errorCode = 0 });
                    else
                    {
                        return Content(HttpStatusCode.BadRequest, new
                        {
                            errorMessage = HelperClass.GetEnumDescription(ErrorMessages.InternalDbError),
                            errorCode = ErrorMessages.InternalDbError
                        });
                    }
                }
                else
                {
                    return Content(HttpStatusCode.BadRequest, new
                    {
                        errorMessage = HelperClass.GetEnumDescription(ErrorMessages.invalidOtp),
                        errorCode = ErrorMessages.invalidOtp
                    });
                }
            }
            catch (Exception e)
            {
                log4net.LogManager.GetLogger(GetType()).Info(e);
                return Content(HttpStatusCode.InternalServerError, new
                {
                    errorMessage = HelperClass.GetEnumDescription(ErrorMessages.error_occured),
                    errorCode = ErrorMessages.error_occured
                });
            }
        }
    }
}