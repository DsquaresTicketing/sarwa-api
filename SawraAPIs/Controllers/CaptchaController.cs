﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using Newtonsoft.Json;
using BotDetect.Web;
using SawraAPIs.Models.requestObjects;

namespace SawraAPIs.Controllers
{
    public class CaptchaController : ApiController
    {
        [HttpPost]
        public HttpResponseMessage Post(CatptchaModel value)
        {
            string userEnteredCaptchaCode = value.UserEnteredCaptchaCode;
            string captchaId = value.CaptchaId;

            // create a captcha instance to be used for the captcha validation
            SimpleCaptcha yourFirstCaptcha = new SimpleCaptcha();
            // execute the captcha validation
            bool isHuman = yourFirstCaptcha.Validate(userEnteredCaptchaCode, captchaId);

            if (isHuman == false)
            {
                // captcha validation failed; notify the frontend 
                // TODO: consider logging the attempt
                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
                response.Content = new StringContent("{\"success\":false}", Encoding.UTF8, "application/json");
                return response;
            }
            else
            {
                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
                response.Content = new StringContent("{\"success\":true}", Encoding.UTF8, "application/json");
                return response;
                // TODO: captcha validation succeeded; execute the protected action
                // TODO: do not forget to notify the frontend about the results
            }
        }
    }
}