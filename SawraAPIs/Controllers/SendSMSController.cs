﻿using Loyalty360.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SawraAPIs.Controllers
{
    public class SendSMSController : ApiController
    {
      

        // POST api/<controller>
        [HttpPost]
        public void Post([FromBody]string mobileNo)
        {
            try
            {
              
                Random generator = new Random();
                String unicode = generator.Next(0, 999999).ToString("D6");
                string msg = "Hello in sarwa system";
                SMSService smsService = new SMSService(mobileNo, msg, unicode);
                var x = smsService.SendSMS();
            }
            catch (Exception) { }
        }

       
    }
}