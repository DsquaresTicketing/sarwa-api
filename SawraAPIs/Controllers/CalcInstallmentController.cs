﻿using Newtonsoft.Json;
using Sarwa.App_Start;
using SawraAPIs.Enums;
using SawraAPIs.FilterAction;
using SawraAPIs.HelperClassess;
using SawraAPIs.Models.requestObjects;
using SawraAPIs.Models.responseObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;

namespace Sarwa.Controllers
{

//[EnableCors(origins: "*", headers: "*", methods: "*")]

    public class CalcInstallmentController : ApiController
    {
       [HttpPost]
        [SessionRefresherFilter]
        public IHttpActionResult CalculateInstallment([FromBody]ItemLoan calcObj, string sessionId)
        {
            try
            {
                calcObj.sessionId = sessionId;
                var Data = WebClientConfig.ManageAPI("calcInstallment", calcObj);
                var calObj = JsonConvert.DeserializeObject<CalcInstallments>(Data);
                if(calObj == null) {
                    var error = JsonConvert.DeserializeObject<ErrorResponse>(Data);
                    return Content(HttpStatusCode.BadRequest, error);
                }
                return Content(HttpStatusCode.OK, new { calObj.installmentValue , errorCode = 0});

            }
            catch (Exception e)
            {

                return Content(HttpStatusCode.InternalServerError, new{
                        errorMessage = HelperClass.GetEnumDescription(ErrorMessages.error_occured),
                        errorCode = ErrorMessages.error_occured
                    });
            } 
        }
    }
}