namespace SawraAPIs.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class changeintolong_27012020 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.InvoiceDetails", "InstallmentSeqNo", c => c.Long(nullable: false));
            AlterColumn("dbo.InvoiceDetails", "RefundNo", c => c.Long());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.InvoiceDetails", "RefundNo", c => c.Int());
            AlterColumn("dbo.InvoiceDetails", "InstallmentSeqNo", c => c.Int(nullable: false));
        }
    }
}
