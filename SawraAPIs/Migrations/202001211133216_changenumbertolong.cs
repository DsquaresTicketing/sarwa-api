namespace SawraAPIs.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class changenumbertolong : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.InvoiceDetails", "InvoiceNo", "dbo.Invoices");
            DropIndex("dbo.InvoiceDetails", new[] { "InvoiceNo" });
            DropPrimaryKey("dbo.Invoices");
            AlterColumn("dbo.InvoiceDetails", "InvoiceNo", c => c.Long(nullable: false));
            AlterColumn("dbo.Invoices", "InvoiceNo", c => c.Long(nullable: false));
            AddPrimaryKey("dbo.Invoices", "InvoiceNo");
            CreateIndex("dbo.InvoiceDetails", "InvoiceNo");
            AddForeignKey("dbo.InvoiceDetails", "InvoiceNo", "dbo.Invoices", "InvoiceNo", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.InvoiceDetails", "InvoiceNo", "dbo.Invoices");
            DropIndex("dbo.InvoiceDetails", new[] { "InvoiceNo" });
            DropPrimaryKey("dbo.Invoices");
            AlterColumn("dbo.Invoices", "InvoiceNo", c => c.Int(nullable: false));
            AlterColumn("dbo.InvoiceDetails", "InvoiceNo", c => c.Int(nullable: false));
            AddPrimaryKey("dbo.Invoices", "InvoiceNo");
            CreateIndex("dbo.InvoiceDetails", "InvoiceNo");
            AddForeignKey("dbo.InvoiceDetails", "InvoiceNo", "dbo.Invoices", "InvoiceNo", cascadeDelete: true);
        }
    }
}
