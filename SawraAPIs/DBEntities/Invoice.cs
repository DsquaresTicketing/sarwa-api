﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SawraAPIs.DBEntities
{
    public class Invoice
    {
        [Key]
        public long InvoiceNo { get; set; }
        public decimal Price { get; set; }
        public decimal DownPayment { get; set; }
        public string ClientId { get; set; }
        public DateTime CreationDate { get; set; }
        public int Status { get; set; }
        public List<InvoiceDetails> InvoiceDetailsList { get; set; }
    }
}