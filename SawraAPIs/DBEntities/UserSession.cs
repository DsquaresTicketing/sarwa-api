﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SawraAPIs.DBEntities
{
    public class UserSession
    {   [Key]
        public int Id { get; set; }
        public string SessionId { get; set; }
        public string Username { get; set; }
        public DateTime ExpiryTime { get; set; }
        public bool IsExpired   { get; set; }
    }
}