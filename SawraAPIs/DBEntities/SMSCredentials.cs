﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SawraAPIs.DBEntities
{
    public class SMSCredentials
    {
        
        public int id { get; set; }
        public string mobileNo { get; set; }
        public string code { get; set; }
        public DateTime expirationDate { get; set; }
        public bool isValid { get; set; }
    }
}